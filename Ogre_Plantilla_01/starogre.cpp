#include "stdafx.h"
#include <random>
#include <vector>

using namespace std;

#define ARWING_WIDTH 500
#define ARWING_DEPTH 273
#define ARWING_HEIGHT 100

#define WOLFEN_WIDTH 500
#define WOLFEN_DEPTH 273
#define WOLFEN_HEIGHT 100
#define WOLFEN_SQUAD 5

#define RING_WIDTH 500
#define RING_DEPTH 70
#define RING_HEIGHT 500

#define COIN_WIDTH 250
#define COIN_DEPTH 70
#define COIN_HEIGHT 250

#define METEOR_WIDTH 270 * 4
#define METEOR_DEPTH 90 * 4
#define METEOR_HEIGHT 270 * 4

#define THIN_WIDTH 1112
#define THIN_DEPTH 2514
#define WIDE_WIDTH 2514
#define WIDE_DEPTH 1112
#define BUILD_HEIGHT 10006

bool checkBorde(Ogre::Vector3 pos){

	if( (pos.x<10000) && (pos.x>-10000) &&
		(pos.y< 4890) && (pos.y >-5010) &&
		(pos.z< 89750))
		return true;

	return false;
}

class FrameListenerProyectos : public Ogre::FrameListener{
private:
	OIS::InputManager* _man;
	OIS::Keyboard* _key;
	OIS::Mouse* _mouse;
	Ogre::Camera* _cam;
	std::vector<Ogre::SceneNode**> wolfen;
	Ogre::SceneNode* ship;

public:

	FrameListenerProyectos(Ogre::RenderWindow* win, Ogre::Camera* Cam, Ogre::SceneNode* shi, std::vector<Ogre::SceneNode**> wolf){
		//Conf captura mouse y teclado
		size_t windowHnd = 0;
		std::stringstream windowHndStr;
		win->getCustomAttribute("WINDOW",&windowHnd);
		windowHndStr << windowHnd;

		//Eventos
		OIS::ParamList pl;
		pl.insert(std::make_pair(std::string("WINDOW"), windowHndStr.str()));
		_man = OIS::InputManager::createInputSystem(pl);
		_key = static_cast<OIS::Keyboard*>(_man->createInputObject(OIS::OISKeyboard,false));
		_mouse = static_cast<OIS::Mouse*>(_man->createInputObject(OIS::OISMouse,false));
		//Fin eventos

		_cam = Cam;
		wolfen= wolf;
		ship = shi;
	}

	~FrameListenerProyectos(){
		_man->destroyInputObject(_key);
		_man->destroyInputObject(_mouse);
		OIS::InputManager::destroyInputSystem(_man);
	}
		
	bool frameStarted(const Ogre::FrameEvent& evt){
		_key->capture();
		_mouse->capture();
		float movSpeed = 250.0f;

		if(_key->isKeyDown(OIS::KC_ESCAPE))
			return false;

		Ogre::Vector3 t(0,0,0);

		if(_key->isKeyDown(OIS::KC_W))
			t += Ogre::Vector3(0,0,-10);

		if(_key->isKeyDown(OIS::KC_S))
			t += Ogre::Vector3(0,0,10);

		if(_key->isKeyDown(OIS::KC_A))
			t += Ogre::Vector3(-10,0,0);

		if(_key->isKeyDown(OIS::KC_D))
			t += Ogre::Vector3(10,0,0);

		if(_key->isKeyDown(OIS::KC_SPACE))
			ship->translate(Ogre::Vector3(0,0,25)*evt.timeSinceLastFrame*100);

		float rotX = _mouse->getMouseState().X.rel * evt.timeSinceLastFrame * -1;
		float rotY = _mouse->getMouseState().Y.rel * evt.timeSinceLastFrame * -1;
		_cam->yaw(Ogre::Radian(rotX));
		_cam->pitch(Ogre::Radian(rotY));
		_cam->moveRelative(t*evt.timeSinceLastFrame*movSpeed);
		
//		std::cout<<_cam->getPosition()<<std::endl;

		for(int j = 0;j<wolfen.size();j++){
			int par = j % 2 ? -1 : 1;
			for(int i = 0;i<5; i++){
				wolfen[j][i]->translate(Ogre::Vector3(par*1000,1000,0)*evt.timeSinceLastFrame);
			}
		}

		return true;
	}

	bool frameEnded(const Ogre::FrameEvent& evt){
		return true;
	}

};

class FrameListenerNave : public Ogre::FrameListener{
private:
	std::vector<Ogre::SceneNode**> wolfen;
	OIS::InputManager* _man;
	OIS::Keyboard* _key;
	OIS::Mouse* _mouse;
	Ogre::Camera* _cam;
	Ogre::SceneNode* ship;
	float rotation;
	float inclinacion;
	int velocity;
public:

	FrameListenerNave(Ogre::RenderWindow* win, Ogre::Camera* Cam, Ogre::SceneNode* chip, std::vector<Ogre::SceneNode**>	 wolf){
		//Conf captura mouse y teclado
		size_t windowHnd = 0;
		std::stringstream windowHndStr;
		win->getCustomAttribute("WINDOW",&windowHnd);
		windowHndStr << windowHnd;

		//Eventos
		OIS::ParamList pl;
		pl.insert(std::make_pair(std::string("WINDOW"), windowHndStr.str()));
		_man = OIS::InputManager::createInputSystem(pl);
		_key = static_cast<OIS::Keyboard*>(_man->createInputObject(OIS::OISKeyboard,false));
		_mouse = static_cast<OIS::Mouse*>(_man->createInputObject(OIS::OISMouse,false));
		//Fin eventos

		rotation = 0.0f;
		inclinacion = 0.0f;
		velocity = 25;
		ship  = chip;
		_cam = Cam;
		wolfen = wolf;
	}

	~FrameListenerNave(){
		_man->destroyInputObject(_key);
		_man->destroyInputObject(_mouse);
		OIS::InputManager::destroyInputSystem(_man);
	}
		
	bool frameStarted(const Ogre::FrameEvent& evt){
		_key->capture();
		_mouse->capture();
		float movSpeed = 100.0f;
		
		if(_key->isKeyDown(OIS::KC_ESCAPE))
			return false;

		Ogre::Vector3 t(0,0,0);
		bool move = false, movep = false;


		if(_key->isKeyDown(OIS::KC_W))
			if(checkBorde(ship->getPosition()+Ogre::Vector3(0,0,10))){
				t += Ogre::Vector3(0,0,velocity);
			}else{
				if(ship->getPosition().z>89730)
					ship->getChild("cps")->setPosition(_cam->getPosition()+Ogre::Vector3(0,2,65));
			}
		if(_key->isKeyDown(OIS::KC_S))
			t += Ogre::Vector3(0,0,-velocity);

		if(_key->isKeyDown(OIS::KC_A)){
			if(checkBorde(ship->getPosition()+Ogre::Vector3(10,0,0))){
				t += Ogre::Vector3(velocity,0,0);
				rotation -= 0.005f;
				if(rotation<-0.76f)
					rotation = -0.76f;
				move=true;
				}
			}
		if(_key->isKeyDown(OIS::KC_D)){
			if(checkBorde(ship->getPosition()+Ogre::Vector3(-10,0,0))){
				t += Ogre::Vector3(-velocity,0,0);
				rotation += 0.005f;
				if(rotation>0.76f)
					rotation = 0.76f;
				move=true;
			}
		}
		if(_key->isKeyDown(OIS::KC_UP)){
			if(checkBorde(ship->getPosition()+Ogre::Vector3(0,10,0))){
				t += Ogre::Vector3(0,velocity,0);
				inclinacion -= 0.005f;
				if(inclinacion<-0.30f)
					inclinacion = -0.30f;
				movep=true;
			}
		}
		if(_key->isKeyDown(OIS::KC_DOWN)){
			if(checkBorde(ship->getPosition()+Ogre::Vector3(0,-10,0))){
				t += Ogre::Vector3(0,-velocity,0);
				inclinacion += 0.005f;
				if(inclinacion>0.30f)
					inclinacion = 0.30f;
				movep=true;
			}
		}


		float rotX = _mouse->getMouseState().X.rel * evt.timeSinceLastFrame * -1;
		float rotY = _mouse->getMouseState().Y.rel * evt.timeSinceLastFrame * -1;
		ship->translate(t*evt.timeSinceLastFrame*movSpeed);
		if(!move){
			if(rotation>0.06f)
				rotation-=0.005f;
			else if (rotation<-0.06f)
				rotation+=0.005f;
		}
		if(!movep){
			if(inclinacion>0.06f)
				inclinacion-=0.005f;
			else if (inclinacion<-0.06f)
				inclinacion+=0.005f;
		}
		ship->getChild("ship")->resetOrientation();
		Quaternion rot(Degree(90),Vector3::UNIT_X);
		ship->getChild("ship")->rotate(rot);
		ship->getChild("ship")->yaw(Ogre::Radian(rotation));
		ship->getChild("ship")->pitch(Ogre::Radian(inclinacion));

		for(int j = 0;j<wolfen.size();j++){
			int par = j % 2 ? -1 : 1;
			for(int i = 0;i<5; i++){
				if(wolfen[j][i]->getPosition().z-ship->getPosition().z<4000){
					wolfen[j][i]->translate(Ogre::Vector3(par*2000,2500,0)*evt.timeSinceLastFrame);
				}
			}
		}

		return true;
	}

	bool frameEnded(const Ogre::FrameEvent& evt){
		return true;
	}

};

class FrameListenerObjetos : public Ogre::FrameListener{
private:
	std::vector<Ogre::SceneNode*> rings;
	std::vector<Ogre::SceneNode*> coins;
	std::vector<Ogre::SceneNode*> meteors;
	std::vector<Ogre::SceneNode*> wideBuildings;
	std::vector<Ogre::SceneNode*> thinBuildings;
	std::vector<Ogre::SceneNode**> wolfens;
	Ogre::SceneNode* ship;
	Ogre::Timer invincibleTimer;
	bool invincible;
	int numRings;
	int lives;
	int score;

public:

	FrameListenerObjetos(Ogre::RenderWindow* win, Ogre::SceneNode* chip, std::vector<Ogre::SceneNode*> r, 
						std::vector<Ogre::SceneNode*> w, std::vector<Ogre::SceneNode*> t,
						std::vector<Ogre::SceneNode*> c, std::vector<Ogre::SceneNode*> m,
						std::vector<Ogre::SceneNode**> wol){
		//Conf ventana
		size_t windowHnd = 0;
		std::stringstream windowHndStr;
		win->getCustomAttribute("WINDOW",&windowHnd);
		windowHndStr << windowHnd;

		ship  = chip;
		rings = r;
		wideBuildings = w;
		thinBuildings = t;
		meteors = m;
		coins = c;
		wolfens = wol;
		lives = 3;
		score = 0;
		numRings = r.size();
		invincible = false;
		invincibleTimer.reset();
	}

	~FrameListenerObjetos(){
	}
		
	bool frameStarted(const Ogre::FrameEvent& evt){

		for(int i=0;i<rings.size();++i){
			if(ringCollide(rings[i])){
				rings[i]->setVisible(false);	
				rings.erase(rings.begin()+i);
				--i;
				score += 200;
				std::cout << "Score: " << score << std::endl;
			}
		}

		for(int i=0;i<coins.size();++i){
			if(coinCollide(coins[i])){
				coins[i]->setVisible(false);
				coins.erase(coins.begin()+i);
				--i;
				score += 100;
				std::cout << "Score: " << score << std::endl;
			}
		}

		for(int i=0;i<meteors.size();++i){
			if(meteors[i]->getPosition().z - ship->getPosition().z < 6000){
				meteors[i]->translate(Ogre::Vector3(0,-4000,-4000) * evt.timeSinceLastFrame);
			}
		}

		if(invincible){
			if(invincibleTimer.getMilliseconds() > 3000){
				invincible = false;
			}
		}else{
			for(int i=0;i<wideBuildings.size();++i){
				if(invincible) break;
				if(wideCollide(wideBuildings[i])){
					loseLife();
					if(isDead()) return false;
				}
			}
			for(int i=0;i<thinBuildings.size();++i){
				if(invincible) break;
				if(thinCollide(thinBuildings[i])){
					loseLife();
					if(isDead()) return false;
				}
			}
			for(int i=0;i<meteors.size();++i){
				if(invincible) break;
				if(meteorCollide(meteors[i])){
					meteors[i]->setVisible(false);
					meteors.erase(meteors.begin()+i);
					--i;
					loseLife();
					if(isDead()) return false;
				}
			}
			for(int i=0;i<wolfens.size();++i){
				if(invincible) break;
				for(int j=0;j<WOLFEN_SQUAD;++j){
					if(wolfenCollide(wolfens[i][j])){
						loseLife();
						if(isDead()) return false;
						break;
					}
				}
			}
		}

		return true;
	}

	void loseLife(){
		invincible = true;
		std::cout << "Life lost. " << --lives << " remain" << std::endl;
		invincibleTimer.reset();
	}

	bool isDead(){
		return !(lives);
	}

	bool ringCollide(Ogre::SceneNode* ring){
		return collideWithShip(ring,RING_WIDTH,RING_HEIGHT,RING_DEPTH);
	}

	bool coinCollide(Ogre::SceneNode* coin){
		return collideWithShip(coin,COIN_WIDTH,COIN_HEIGHT,COIN_DEPTH);
	}

	bool meteorCollide(Ogre::SceneNode* met){
		return collideWithShip(met,METEOR_WIDTH,METEOR_HEIGHT,METEOR_DEPTH);
	}

	bool wideCollide(Ogre::SceneNode* build){
		return collideWithShip(build,WIDE_WIDTH,BUILD_HEIGHT,WIDE_DEPTH);
	}

	bool wolfenCollide(Ogre::SceneNode* wol){
		return collideWithShip(wol,WOLFEN_WIDTH,WOLFEN_HEIGHT,WOLFEN_DEPTH);
	}

	bool thinCollide(Ogre::SceneNode* build){
		return collideWithShip(build,THIN_WIDTH,BUILD_HEIGHT,THIN_DEPTH);
	}
		
	bool collideWithShip(Ogre::SceneNode* n, int width, int height, int depth){
		if(!collideX(n,width)) return false;
		if(!collideY(n,height)) return false;
		if(!collideZ(n,depth)) return false;
		return true;
	}

	bool collideX(Ogre::SceneNode* n,int width){
		Ogre::Vector3 rPos = n->_getDerivedPosition();
		Ogre::Vector3 sPos = ship->_getDerivedPosition();

		return !(rPos.x + width/2 < sPos.x - ARWING_WIDTH/2 || rPos.x - width/2 > sPos.x + ARWING_WIDTH/2);
	}

	bool collideY(Ogre::SceneNode* n,int height){
		Ogre::Vector3 rPos = n->_getDerivedPosition();
		Ogre::Vector3 sPos = ship->_getDerivedPosition();

		return !(rPos.y + height/2 < sPos.y - ARWING_HEIGHT/2 || rPos.y - height/2 > sPos.y + ARWING_HEIGHT/2);
	}

	bool collideZ(Ogre::SceneNode* n,int depth){
		Ogre::Vector3 rPos = n->_getDerivedPosition();
		Ogre::Vector3 sPos = ship->_getDerivedPosition();

		return !(rPos.z + depth/2 < sPos.z - ARWING_DEPTH/2 || rPos.z - depth/2 > sPos.z + ARWING_DEPTH/2);
	}

	bool frameEnded(const Ogre::FrameEvent& evt){
		return true;
	}

};


class OgreProyectos{

private:
	Ogre::SceneManager* _sceneManager;
	Ogre::Root* _root;
	FrameListenerNave* _listener;
	FrameListenerObjetos* _objetosListener;
//	FrameListenerProyectos* _listener;
	Ogre::SceneNode* nM01;
	std::vector<Ogre::SceneNode*> aros;
	std::vector<Ogre::SceneNode*> monedas;
	std::vector<Ogre::SceneNode*> delgados;
	std::vector<Ogre::SceneNode*> anchos;
	std::vector<Ogre::SceneNode*> meteoros;
	Ogre::Camera* camera;
	std::vector<Ogre::SceneNode**> wolfen;

public:

	void drawWolfen(Ogre::SceneNode* nM01, int i,int j){
		Ogre::Entity* entMesh01 = _sceneManager->createEntity("wolfen"+Ogre::StringConverter::toString(j)+Ogre::StringConverter::toString(i),"metal.mesh");
		entMesh01->setMaterialName("metal");
		nM01->attachObject(entMesh01);
		entMesh01 = _sceneManager->createEntity("wolfen_plate"+Ogre::StringConverter::toString(j)+Ogre::StringConverter::toString(i),"metal plate.mesh");
		entMesh01->setMaterialName("plate");
		nM01->attachObject(entMesh01);
		entMesh01 = _sceneManager->createEntity("wolfen_bars"+Ogre::StringConverter::toString(j)+Ogre::StringConverter::toString(i),"metal bars.mesh");
		entMesh01->setMaterialName("bars");
		nM01->attachObject(entMesh01);
		entMesh01 = _sceneManager->createEntity("wolfen_glass"+Ogre::StringConverter::toString(j)+Ogre::StringConverter::toString(i),"metal glass.mesh");
		entMesh01->setMaterialName("glass");
		nM01->attachObject(entMesh01);
		entMesh01 = _sceneManager->createEntity("wolfen_red"+Ogre::StringConverter::toString(j)+Ogre::StringConverter::toString(i),"metal red.mesh");
		entMesh01->setMaterialName("red");
		nM01->attachObject(entMesh01);
		entMesh01 = _sceneManager->createEntity("wolfen_vent"+Ogre::StringConverter::toString(j)+Ogre::StringConverter::toString(i),"metal vent.mesh");
		entMesh01->setMaterialName("vent");
		nM01->attachObject(entMesh01);
		entMesh01 = _sceneManager->createEntity("wolfen_wing"+Ogre::StringConverter::toString(j)+Ogre::StringConverter::toString(i),"metal wing.mesh");
		entMesh01->setMaterialName("wing");
		nM01->attachObject(entMesh01);
}

	void drawArwing(Ogre::SceneNode* nM01){
		Ogre::Entity* entMesh01 = _sceneManager->createEntity("arwing","arwing1.mesh");
		entMesh01->setMaterialName("airwing");
		nM01->attachObject(entMesh01);
		entMesh01 = _sceneManager->createEntity("entMesh02","arwing2.mesh");
		entMesh01->setMaterialName("airwing");
		nM01->attachObject(entMesh01);
		entMesh01 = _sceneManager->createEntity("entMesh03","arwing3.mesh");
		entMesh01->setMaterialName("airwing");
		nM01->attachObject(entMesh01);
		entMesh01 = _sceneManager->createEntity("entMesh04","arwing4.mesh");
		entMesh01->setMaterialName("airwing");
		nM01->attachObject(entMesh01);
		entMesh01 = _sceneManager->createEntity("entMesh05","arwing5.mesh");
		entMesh01->setMaterialName("airwing");
		nM01->attachObject(entMesh01);
		entMesh01 = _sceneManager->createEntity("entMesh06","arwing6.mesh");
		entMesh01->setMaterialName("airwing");
		nM01->attachObject(entMesh01);
		Ogre::SceneNode* weed = _sceneManager->createSceneNode("smokwing");
		Ogre::ParticleSystem* partSystem = _sceneManager->createParticleSystem("Smoke","Turbine");
        weed->attachObject(partSystem);
		weed->setPosition(nM01->getPosition()+Ogre::Vector3(0,-75,0));
		nM01->addChild(weed);
		nM01->scale(1.25f,1.0f,1.5f);
	}

	void drawAro(Ogre::SceneNode* aro, Ogre::String tag){
		Ogre::Entity* aroMesh = _sceneManager->createEntity("elaro"+tag,"poly04.mesh");
		_sceneManager->getRootSceneNode()->addChild(aro);
		aro->attachObject(aroMesh);
		aro->scale(3.5f,3.5f,3.5f);
		Quaternion rt(Degree(90),Vector3::UNIT_X);
		aro->rotate(rt);
		aroMesh->setMaterialName("aro");
	}

	void drawMeteoro(Ogre::SceneNode* met, Ogre::String tag){
		Ogre::Entity* entM = _sceneManager->createEntity("meteoro" + tag,"poly18.mesh");
		_sceneManager->getRootSceneNode()->addChild(met);
		met->attachObject(entM);
		entM->setMaterialName("rust");
		met->scale(4,4,4);
	}

	void drawMontana(Ogre::SceneNode* mt, Ogre::String tag){
		Ogre::Entity* entM = _sceneManager->createEntity("mt" + tag,"poly05.mesh");
		_sceneManager->getRootSceneNode()->addChild(mt);
		mt->attachObject(entM);
		entM->setMaterialName("terrain");
		mt->scale(30,100,30);
	}

	void drawMoneda(Ogre::SceneNode* coin, Ogre::String tag){
		Ogre::Entity* coinMesh = _sceneManager->createEntity("moneda"+tag,"poly14.mesh");
		_sceneManager->getRootSceneNode()->addChild(coin);
		coin->attachObject(coinMesh);
		coin->scale(1.5f,1.5f,1.5f);
		Quaternion rt(Degree(90),Vector3::UNIT_X);
		coin->rotate(rt);
		coinMesh->setMaterialName("moneda");
	}

	OgreProyectos(){
		_sceneManager = NULL;
		_root = NULL;
		_listener = NULL;
	}

	~OgreProyectos(){
		delete( _listener);
		delete(_root);
	}

	void loadResources(){
		Ogre::ConfigFile cf;
		cf.load("recursos_propios.cfg");

		Ogre::ConfigFile::SectionIterator sectionIter = cf.getSectionIterator();
		Ogre::String sectionName, typeName, dataname;
		while(sectionIter.hasMoreElements()){
			sectionName = sectionIter.peekNextKey();
			Ogre::ConfigFile::SettingsMultiMap* settings = sectionIter.getNext();
			Ogre::ConfigFile::SettingsMultiMap::iterator i;
			for(i = settings->begin();i != settings->end(); i++){
				typeName = i->first;
				dataname = i->second;
				Ogre::ResourceGroupManager::getSingleton().addResourceLocation(dataname,typeName,sectionName);
			}

			Ogre::ResourceGroupManager::getSingleton().initialiseAllResourceGroups();
		}
	}

	int startup()	{
		_root = new Ogre::Root("plugins_d.cfg");

		if(!_root->showConfigDialog()){
			return -1;
		}

		Ogre::RenderWindow* window = _root->initialise(true,"Ventana Ogre");
		_sceneManager = _root->createSceneManager(Ogre::ST_GENERIC);

		camera = _sceneManager->createCamera("Camera");
		camera->setPosition(Ogre::Vector3(5,238,-1315));
//		camera->setPosition(Ogre::Vector3(-1500,238,0));
		camera->lookAt(Ogre::Vector3(0,0,0));
		camera->setNearClipDistance(5);

		Ogre::Viewport* viewport = window->addViewport(camera);
		viewport->setBackgroundColour(Ogre::ColourValue(0.0,0.0,0.0));
		camera->setAspectRatio(Ogre::Real(viewport->getActualWidth()/viewport->getActualHeight()));

		loadResources();
		createScene();


		_listener = new FrameListenerNave(window,camera,nM01,wolfen);
//		_listener = new FrameListenerProyectos(window,camera,nM01,wolfen);
		_objetosListener = new FrameListenerObjetos(window,nM01,aros,anchos,delgados,monedas,meteoros,wolfen);
		_root->addFrameListener(_listener);
		_root->addFrameListener(_objetosListener);

		_root->startRendering();

		return 0;
	}

	void createScene(){
		
		_sceneManager->setAmbientLight(Ogre::ColourValue(0.9f,0.9f,0.9f));

		//START
		Ogre::Plane planes(-Vector3::UNIT_Z, 0);
		Ogre::MeshManager::getSingleton().createPlane("sa",
			ResourceGroupManager::DEFAULT_RESOURCE_GROUP_NAME, planes,
			25,12.5,20,20,true,1,1,1,Vector3::UNIT_Y);
		Ogre::Entity* enti = _sceneManager->createEntity("sta",
			"sa");
		Ogre::SceneNode* plans = _sceneManager->createSceneNode("sts");
		plans->attachObject(enti);
		plans->setPosition(Ogre::Vector3(5,240,-1250));
		_sceneManager->getRootSceneNode()->addChild(plans);
		enti->setMaterialName("start");

		//COMPLETE
		Ogre::Plane pls(-Vector3::UNIT_Z, 0);
		Ogre::MeshManager::getSingleton().createPlane("cp",
			ResourceGroupManager::DEFAULT_RESOURCE_GROUP_NAME, pls,
			25,12.5,20,20,true,1,1,1,Vector3::UNIT_Y);
		Ogre::Entity* eni = _sceneManager->createEntity("cpm",
			"cp");
		Ogre::SceneNode* plas = _sceneManager->createSceneNode("cps");
		plas->attachObject(eni);
		plas->setPosition(camera->getPosition()+Ogre::Vector3(0,2,-65));
		eni->setMaterialName("complete");

		//LANZADORES
		for(int j=0;j<10;j++){
			int par = j % 2 ? -1 : 1;
			Ogre::SceneNode* nM04 = _sceneManager->createSceneNode("lanz"+Ogre::StringConverter::toString(j));
			Ogre::Entity* entMesh04 = _sceneManager->createEntity("lanzador"+Ogre::StringConverter::toString(j),"poly07.mesh");
			_sceneManager->getRootSceneNode()->addChild(nM04);
			nM04->attachObject(entMesh04);
			nM04->setPosition(par*-5000,-4750,8000+j*8000);
			entMesh04->setMaterialName("lanz");
			Quaternion rota = Quaternion(Degree(120),par*Vector3::UNIT_Z);
			nM04->rotate(rota);
			nM04->scale(10.0f,10.0f,10.0f);

			wolfen.push_back(new Ogre::SceneNode*[5]);
			for(int i=0; i<5; i++){
				wolfen[j][i] = _sceneManager->createSceneNode("wolf"+Ogre::StringConverter::toString(j)+Ogre::StringConverter::toString(i));
				_sceneManager->getRootSceneNode()->addChild(wolfen[j][i]);
				drawWolfen(wolfen[j][i],i,j);
				rota = Quaternion(Degree(90),par*Vector3::UNIT_Y);
				wolfen[j][i]->rotate(rota);
				rota = Quaternion(Degree(60),Vector3::UNIT_X);
				wolfen[j][i]->rotate(rota);
				wolfen[j][i]->scale(2.0f,2.0f,2.0f);	
			}

			for(int i=0; i<3; i++)
				wolfen[j][i]->setPosition(nM04->getPosition()+Ogre::Vector3(i*150,i*50,i*150));
			for(int i=3; i<5; i++)
				wolfen[j][i]->setPosition(nM04->getPosition()+Ogre::Vector3((i-2)*150,-(i-2)*50,-(i-2)*150));
		}

		for(int j=10;j<20;j++){
			int par = 1;
			Ogre::SceneNode* nM04 = _sceneManager->createSceneNode("lanz"+Ogre::StringConverter::toString(j));
			Ogre::Entity* entMesh04 = _sceneManager->createEntity("lanzador"+Ogre::StringConverter::toString(j),"poly07.mesh");
			_sceneManager->getRootSceneNode()->addChild(nM04);
			nM04->attachObject(entMesh04);
			nM04->setPosition(par*-10000,-4750,4000+(j-10)*8000);
			entMesh04->setMaterialName("lanz");
			Quaternion rota = Quaternion(Degree(120),par*Vector3::UNIT_Z);
			nM04->rotate(rota);
			nM04->scale(10.0f,10.0f,10.0f);

			wolfen.push_back(new Ogre::SceneNode*[5]);
			for(int i=0; i<5; i++){
				wolfen[j][i] = _sceneManager->createSceneNode("wolf"+Ogre::StringConverter::toString(j)+Ogre::StringConverter::toString(i));
				_sceneManager->getRootSceneNode()->addChild(wolfen[j][i]);
				drawWolfen(wolfen[j][i],i,j);
				rota = Quaternion(Degree(90),par*Vector3::UNIT_Y);
				wolfen[j][i]->rotate(rota);
				rota = Quaternion(Degree(60),Vector3::UNIT_X);
				wolfen[j][i]->rotate(rota);
				wolfen[j][i]->scale(2.0f,2.0f,2.0f);	
			}

			for(int i=0; i<3; i++)
				wolfen[j][i]->setPosition(nM04->getPosition()+Ogre::Vector3(i*150,i*50,i*150));
			for(int i=3; i<5; i++)
				wolfen[j][i]->setPosition(nM04->getPosition()+Ogre::Vector3((i-2)*150,-(i-2)*50,-(i-2)*150));
		}

		for(int j=20;j<30;j++){
			int par = -1;
			Ogre::SceneNode* nM04 = _sceneManager->createSceneNode("lanz"+Ogre::StringConverter::toString(j));
			Ogre::Entity* entMesh04 = _sceneManager->createEntity("lanzador"+Ogre::StringConverter::toString(j),"poly07.mesh");
			_sceneManager->getRootSceneNode()->addChild(nM04);
			nM04->attachObject(entMesh04);
			nM04->setPosition(par*-10000,-4750,4000+(j-20)*8000);
			entMesh04->setMaterialName("lanz");
			Quaternion rota = Quaternion(Degree(120),par*Vector3::UNIT_Z);
			nM04->rotate(rota);
			nM04->scale(10.0f,10.0f,10.0f);

			wolfen.push_back(new Ogre::SceneNode*[5]);
			for(int i=0; i<5; i++){
				wolfen[j][i] = _sceneManager->createSceneNode("wolf"+Ogre::StringConverter::toString(j)+Ogre::StringConverter::toString(i));
				_sceneManager->getRootSceneNode()->addChild(wolfen[j][i]);
				drawWolfen(wolfen[j][i],i,j);
				rota = Quaternion(Degree(90),par*Vector3::UNIT_Y);
				wolfen[j][i]->rotate(rota);
				rota = Quaternion(Degree(60),Vector3::UNIT_X);
				wolfen[j][i]->rotate(rota);
				wolfen[j][i]->scale(2.0f,2.0f,2.0f);	
			}

			for(int i=0; i<3; i++)
				wolfen[j][i]->setPosition(nM04->getPosition()+Ogre::Vector3(i*150,i*50,i*150));
			for(int i=3; i<5; i++)
				wolfen[j][i]->setPosition(nM04->getPosition()+Ogre::Vector3((i-2)*150,-(i-2)*50,-(i-2)*150));
		}


		//EDIFICIOS
		Ogre::String matEdif = "edif";
		//creacion de edificios dentro
		std::default_random_engine gen_y,gen_x;
		std::uniform_int_distribution<int> dist_y(-6000,-2000), dist_x(-9000,9000);
		int y,x;
		Ogre::SceneNode* nM03;
		for(int i=1; i<10; i++){
			y = dist_y(gen_y);
			x = dist_x(gen_x);
			nM03 = _sceneManager->createSceneNode("edif"+Ogre::StringConverter::toString(i));
			Ogre::Entity* entMesh03 = _sceneManager->createEntity("edificio"+Ogre::StringConverter::toString(i),"poly02.mesh");
			_sceneManager->getRootSceneNode()->addChild(nM03);
			nM03->attachObject(entMesh03);
//			nM03->setPosition(0,-2000,8000.0f*i);
			nM03->setPosition(x,y,8000.0f*i);
			if(i%2){
				nM03->scale(25.0f,50.0f,10.0f);
				anchos.push_back(nM03);
			}else{
				nM03->scale(10.0f,50.0f,25.0f);
				delgados.push_back(nM03);
			}
			entMesh03->setMaterialName(matEdif);
		}

		std::uniform_int_distribution<int>* dx = new std::uniform_int_distribution<int>(25000,50000);
		for(int i=1; i<23; i++){
			y = dist_y(gen_y);
			x = dx->operator()(gen_x);
			nM03 = _sceneManager->createSceneNode("edifi"+Ogre::StringConverter::toString(i));
			Ogre::Entity* entMesh03 = _sceneManager->createEntity("edificioi"+Ogre::StringConverter::toString(i),"poly02.mesh");
			_sceneManager->getRootSceneNode()->addChild(nM03);
			nM03->attachObject(entMesh03);
//			nM03->setPosition(0,-2000,8000.0f*i);
			nM03->setPosition(x,y,4000.0f*i);
				if(i%2)
				nM03->scale(25.0f,50.0f,10.0f);
			else
				nM03->scale(10.0f,50.0f,25.0f);
			entMesh03->setMaterialName(matEdif);
		}
		dx = new std::uniform_int_distribution<int>(-50000,-25000);
		for(int i=1; i<23; i++){
			y = dist_y(gen_y);
			x = dx->operator()(gen_x);
			nM03 = _sceneManager->createSceneNode("edifd"+Ogre::StringConverter::toString(i));
			Ogre::Entity* entMesh03 = _sceneManager->createEntity("edificiod"+Ogre::StringConverter::toString(i),"poly02.mesh");
			_sceneManager->getRootSceneNode()->addChild(nM03);
			nM03->attachObject(entMesh03);
//			nM03->setPosition(0,-2000,8000.0f*i);
			nM03->setPosition(x,y,4000.0f*i);
				if(i%2)
				nM03->scale(25.0f,50.0f,10.0f);
			else
				nM03->scale(10.0f,50.0f,25.0f);
			entMesh03->setMaterialName(matEdif);
		}

		//Aros
		std::default_random_engine generator_x, generator_y,generator_z;
		std::uniform_int_distribution<int> distribution_x(-9000,9000), distribution_y(-3000,4000), distribution_z(10000, 80500);
		int x_gen, y_gen, z_gen;
		for(int i=1; i<18; i++){
			x_gen = distribution_x(generator_x);
			y_gen = distribution_y(generator_y);
			Ogre::SceneNode* aro = _sceneManager->createSceneNode("elaro"+Ogre::StringConverter::toString(i));
			drawAro(aro, Ogre::StringConverter::toString(i));
			aro->setPosition(x_gen,y_gen,i*5000.0f);
			aros.push_back(aro);
		}

		//Monedas
		for(int i=1; i<30; i++){
			x_gen = distribution_x(generator_x);
			y_gen = distribution_y(generator_y);
			z_gen = distribution_z(generator_z);
			Ogre::SceneNode* moneda = _sceneManager->createSceneNode("moneda"+Ogre::StringConverter::toString(i));
			drawMoneda(moneda, Ogre::StringConverter::toString(i));
			moneda->setPosition(x_gen,y_gen,z_gen);
			monedas.push_back(moneda);
		}

		//Piso
		Ogre::Plane plane(Vector3::UNIT_Y, -5000);
		Ogre::MeshManager::getSingleton().createPlane("plane",
			ResourceGroupManager::DEFAULT_RESOURCE_GROUP_NAME, plane,
			1000000,1000000,20,20,true,1,1000,1000,Vector3::UNIT_Z);
		Ogre::Entity* ent = _sceneManager->createEntity("LightPlaneEntity",
			"plane");
		Ogre::SceneNode* plan = _sceneManager->createSceneNode("plan");
		plan->attachObject(ent);
		plan->setPosition(0,0,45000);
		_sceneManager->getRootSceneNode()->addChild(plan);
		ent->setMaterialName("floor");

		// Nave
		nM01 = _sceneManager->createSceneNode("aw1");
		_sceneManager->getRootSceneNode()->addChild(nM01);
		SceneNode* chip = _sceneManager->createSceneNode("ship"); 	
		drawArwing(chip);
		nM01->addChild(chip);
		nM01->attachObject(camera);
		nM01->addChild(plas);
		Quaternion rot(Degree(90),Vector3::UNIT_X);
		chip->rotate(rot);

		//METEOROS
		for(int i=0;i<18;++i){
			x_gen = distribution_x(generator_x);
			Ogre::SceneNode* met = _sceneManager->createSceneNode("meteoro"+Ogre::StringConverter::toString(i));
			drawMeteoro(met, Ogre::StringConverter::toString(i));
			met->setPosition(x_gen,5000,(i+1)*5000);
			meteoros.push_back(met);
		}

		//MONTANAS
		for(int i=0;i<15;++i){
			Ogre::SceneNode* mt = _sceneManager->createSceneNode("mt"+Ogre::StringConverter::toString(i));
			drawMontana(mt, Ogre::StringConverter::toString(i));
			mt->setPosition(-15000,-3000,i*5000);
		}
		for(int i=0;i<15;++i){
			Ogre::SceneNode* mt = _sceneManager->createSceneNode("mt"+Ogre::StringConverter::toString(i+20));
			drawMontana(mt, Ogre::StringConverter::toString(i+20));
			mt->setPosition(15000,-3000,i*5000);
		}

/*		// Borde
		Ogre::SceneNode* nave = _sceneManager->createSceneNode("nave");
		ManualObject* bordeNave = _sceneManager->createManualObject("borde");
		bordeNave->begin("BaseWhiteNoLighting", RenderOperation::OT_LINE_LIST);
		bordeNave->position(-250,-250,250);
		bordeNave->position(250,-250,250);
		bordeNave->position(250,-250,250);
		bordeNave->position(250,250,250);
		bordeNave->position(250,250,250);
		bordeNave->position(-250,250,250);
		bordeNave->position(-250,250,250);
		bordeNave->position(-250,-250,250);

		bordeNave->position(-250,-250,-250);
		bordeNave->position(250,-250,-250);
		bordeNave->position(250,-250,-250);
		bordeNave->position(250,250,-250);
		bordeNave->position(250,250,-250);
		bordeNave->position(-250,250,-250);
		bordeNave->position(-250,250,-250);
		bordeNave->position(-250,-250,-250);

		bordeNave->position(-250,-250,250);
		bordeNave->position(-250,-250,-250);

		bordeNave->position(250,-250,250);
		bordeNave->position(250,-250,-250);

		bordeNave->position(-250,250,250);
		bordeNave->position(-250,250,-250);

		bordeNave->position(250,250,250);
		bordeNave->position(250,250,-250);

		bordeNave->end();
		nave->attachObject(bordeNave);
		_sceneManager->getRootSceneNode()->addChild(nave);

		Ogre::SceneNode* borde = _sceneManager->createSceneNode("borde");
		ManualObject* bordem = _sceneManager->createManualObject("bordem");
		bordem->begin("BaseWhiteNoLighting", RenderOperation::OT_LINE_LIST);
		bordem->position(-10000,-5000,0);
		bordem->position(-10000,5000,0);

		bordem->position(-10000,5000,0);
		bordem->position(10000,5000,0);

		bordem->position(10000,5000,0);
		bordem->position(10000,-5000,0);

		bordem->position(-10000,-5000,0);
		bordem->position(10000,-5000,0);


		bordem->position(-10000,-5000,90000);
		bordem->position(-10000,5000,90000);

		bordem->position(-10000,5000,90000);
		bordem->position(10000,5000,90000);

		bordem->position(10000,5000,90000);
		bordem->position(10000,-5000,90000);

		bordem->position(-10000,-5000,90000);
		bordem->position(10000,-5000,90000);
///////////////////////////////////////////////////////////////
		bordem->position(-10000,-5000,0);
		bordem->position(-10000,-5000,90000);

		bordem->position(-10000,5000,0);
		bordem->position(-10000,5000,90000);

		bordem->position(10000,-5000,0);
		bordem->position(10000,-5000,90000);

		bordem->position(10000,5000,0);
		bordem->position(10000,5000,90000);

		bordem->end();
		borde->attachObject(bordem);
		_sceneManager->getRootSceneNode()->addChild(borde);
		*/

		//Skybox
		_sceneManager->setSkyBox(true, "Examples/SpaceSkyBox");

	}
};

int main(void){
	OgreProyectos app;
	app.startup();
	return 0;
}